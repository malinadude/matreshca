import Vue from 'vue'
import Vuex from 'vuex'
import actions from './functional/actions'
import mutations from './functional/mutations'
import getters from './functional/getters'
import state from "./functional/state";

Vue.use(Vuex);

export default new Vuex.Store({
    state,
    mutations,
    getters,
    actions
})
