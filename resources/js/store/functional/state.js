let state = {
    categories: [],
    tasks: [],
    currentCategory: null,
    currentTask: null,
    modalState: null,
    modalType: null,
}

export default state
