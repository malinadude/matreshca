let getters = {
    categories: state => {
        return state.categories;
    },
    tasks: state => {
        return state.tasks;
    },
    currentCategory: state => {
        return state.currentCategory;
    },
    currentTask: state => {
        return state.currentTask;
    },
    modalType: state => {
        return state.modalType;
    },
    modalState: state => {
        return state.modalState;
    },
}

export default  getters
