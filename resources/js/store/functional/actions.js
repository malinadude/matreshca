import axios from 'axios'

let actions = {
    // Category
    createCategory({commit}, category) {
        axios.post('/api/category', category)
            .then(res => {
                commit('CREATE_CATEGORY', res.data)
            }).catch(err => {
            console.log(err)
        })
    },
    fetchCategories({commit}) {
        this.state.categories = null; // сделано для того, чтобы отображалась надпись "Загрузка..."

        axios.get('/api/category')
            .then(res => {
                commit('FETCH_CATEGORIES', res.data)
            }).catch(err => {
            console.log(err)
        })
    },
    getCurrentCategory({commit}, category_id) {
        axios.get(`/api/category/id/${category_id}`)
            .then(res => {
                commit('GET_CURRENT_CATEGORY', res.data[0])
            }).catch(err => {
            console.log(err)
        })
    },

    // Task
    createTask({commit}, task) {
        axios.post('/api/task/add', task)
            .then(res => {
                commit('CREATE_TASK', res.data)
            }).catch(err => {
            console.log(err)
        })
    },
    fetchTasks({commit}, category_id) {
        this.state.tasks = null; // сделано для того, чтобы отображалась надпись "Загрузка..."

        axios.get(`/api/category/tasks/${category_id}`)
            .then(res => {
                commit('FETCH_TASKS', res.data)
            }).catch(err => {
            console.log(err)
        })
    },
    getCurrentTask({commit}, task_id) {
        axios.get(`/api/task/id/${task_id}`)
            .then(res => {
                commit('GET_CURRENT_TASK', res.data)
            }).catch(err => {
            console.log(err)
        })
    },
    deleteTask({commit}, data) {
        return axios.delete(`/api/task/delete/${data.task.id}`)
            .then(res => {
                if (res.data === 'ok')
                    commit('DELETE_TASK', data.task);

                    if (data.routeToBack) {
                        data.vm.$router.go(-1);
                    }
            }).catch(err => {
            console.log(err)
        })
    },
    changeTask({commit}, task) {
        return axios.post(`/api/task/change`, task)
            .then(res => {
            }).catch(err => {
            console.log(err)
        })
    },

    // Modal
    setModalType({commit}, data) {
        commit('SET_MODAL_TYPE', data);
    },
    changeModalState({commit}, data) {
        commit('CHANGE_MODAL_STATE', data);
    }
}

export default actions
