let mutations = {
    // Category
    CREATE_CATEGORY(state, category) {
        state.categories.unshift(category);
    },
    FETCH_CATEGORIES(state, categories) {
        return state.categories = categories;
    },
    GET_CURRENT_CATEGORY(state, category) {
        state.currentCategory = category;
    },

    // Task
    CREATE_TASK(state, task) {
        state.tasks.unshift(task);
    },
    FETCH_TASKS(state, tasks) {
        return state.tasks = tasks;
    },
    GET_CURRENT_TASK(state, task) {
        state.currentTask = task;
    },
    DELETE_TASK(state, task) {
        let index = state.tasks.findIndex(item => item.id === task.id);

        state.tasks.splice(index, 1);
    },

    // Modal
    SET_MODAL_TYPE(state, data) {
        state.modalType = data;
    },
    CHANGE_MODAL_STATE(state, data) {
        state.modalState = data;
    },
}

export default mutations
