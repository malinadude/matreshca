import Vue from 'vue';
import Router from 'vue-router';
import Categories from '../views/Categories';
import Tasks from '../views/Tasks';
import Task from '../views/Task';

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            name: 'categories',
            path: '/',
            component: Categories,
        },
        {
            name: 'tasks',
            path: '/category/:categoryId',
            component: Tasks,
        },
        {
            name: 'task',
            path: '/category/:categoryId/task/:taskId',
            component: Task,
        },
    ],
});
