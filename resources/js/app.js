// require('./bootstrap');

import Vue from 'vue';
import store from './store';
import router from './router';

import App from './views/App';

new Vue({
    el: '#app',
    components: {
        App,
    },
    store,
    router,
});
