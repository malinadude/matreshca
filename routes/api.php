<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Category
Route::get('category', 'CategoryController@get');
Route::get('category/id/{id}', 'CategoryController@getCategoryById');
Route::post('category', 'CategoryController@store');

//Task
Route::get('category/tasks/{category_id}', 'TaskController@get');
Route::get('task/id/{id}', 'TaskController@getTaskById');
Route::post('task/add', 'TaskController@store');
Route::post('task/change', 'TaskController@change');
Route::delete('task/delete/{id}', 'TaskController@delete');
