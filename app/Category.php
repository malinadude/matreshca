<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Task;

class Category extends Model
{
    public $timestamps = false;

    protected $fillable = [ 'name' ];

    public function tasks()
    {
        return $this->hasMany(Task::Class);
    }
}
