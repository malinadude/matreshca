<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = Category::create($request->all());

        return response()->json($category);
    }

    /**
     * Returns all categories existing in our database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function get()
    {
        $categories = Category::orderBy('id', 'desc')->get();

        return response()->json($categories);
    }

    /**
     * Return category by id existing in our database.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getCategoryById($id)
    {
        $category = Category::find($id)->get();

        return response()->json($category);
    }
}
