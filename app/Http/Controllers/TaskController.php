<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Task;

class TaskController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tasks = Task::create($request->all());

        return response()->json($tasks);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        Task::destroy($id);

        return response()->json("ok");
    }

    /**
     * Change the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function change(Request $request)
    {
        $task = Task::find($request->post('id'));
        $task->status = $request->post('status');
        $task->save();

        return response()->json("ok");
    }

    /**
     * Returns all tasks of category existing in our database.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get($id)
    {
        $tasks = Category::find($id)->tasks()->orderBy('id', 'desc')->get();

        return response()->json($tasks);
    }

    /**
     * Return task by id existing in our database.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getTaskById($id)
    {
        $task = Task::find($id);

        return response()->json($task);
    }
}
