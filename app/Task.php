<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;

class Task extends Model
{
    public $timestamps = false;

    protected $fillable = [ 'name', 'description', 'category_id', 'status' ];

    public function category()
    {
        return $this->hasOne(Category::Class);
    }
}
